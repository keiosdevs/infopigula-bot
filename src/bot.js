const Facecuck = require('fb')
const fs = require('fs')
const https = require('https')
const url = require('url')

const credentials = {
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET
}

const INFOPIGULA_PAGE_ID = "969821293124681"
const POSTS_DIRECTORY = './posts'

const MATTERMOST_MESSAGE_LENGTH_LIMIT = 3500
const MATTERMOST_WEBHOOK = "https://mm.viamage.com/hooks/czo8p5ug87rci85thu87am37rh"

const MATTERMOST_REQUEST_OPTIONS = Object.assign({}, url.parse(MATTERMOST_WEBHOOK),
    {
        method: 'POST',
        headers: {'Content-Type': 'application/json'}
    }
)

Promise.sequence = funcs => funcs.reduce(
    (promise, func) => promise.then(result => func().then(Array.prototype.concat.bind(result))),
    Promise.resolve([])
)

const splitTextToMattermostLimitedMessages = text => [text.split('\n').reduce((obj, curr) =>
        (obj.acc.length + curr.length + 1 > MATTERMOST_MESSAGE_LENGTH_LIMIT) ? {
            acc: curr,
            messages: obj.messages.concat([obj.acc])
        } : {
            acc: obj.acc + curr + "\n",
            messages: obj.messages
        },
    {acc: '', messages: []})].reduce((_, c) => c.messages.concat([c.acc]),[])

const makeMmNotification = text => ({
    username: "Wizard",
    icon_url: "https://uploads.keios.eu/wizard128.png",
    text: text
})

const checkIfExists = path => new Promise((resolve, reject) => {
    fs.stat(path, err => {
        if (err) {
            if (err.code === 'ENOENT')
                resolve(false)
            else if (err.code === 'EEXIST')
                resolve(true)
            else
                reject(err)
        }
        else resolve(true)
    })
})

const checkIfPostExists = postId => checkIfExists(`${POSTS_DIRECTORY}/${postId}`)

const makePostsDirectory =
    checkIfExists(POSTS_DIRECTORY)
        .then(alreadyExists => new Promise((resolve, reject) => {
            if (alreadyExists) resolve()
            else fs.mkdir(POSTS_DIRECTORY, err => {
                if (err) reject(err)
                else resolve()
            })
        }))

const savePost = (postId, postContent) => new Promise((resolve, reject) => {
    fs.writeFile(`${POSTS_DIRECTORY}/${postId}`, postContent, err => {
        if (err) reject(err)
        else resolve()
    })
})

const sendMessageToMattermost = message => new Promise((resolve, reject) => {
    const requestBody = JSON.stringify(makeMmNotification(message))
    const requestOptions = Object.assign({}, MATTERMOST_REQUEST_OPTIONS, {
        'Content-Length': Buffer.byteLength(requestBody)
    })

    const request = https.request(requestOptions, httpResponse => {
        let body = ''
        httpResponse.on('data', buf => {
            body = body + buf
        })
        httpResponse.on('end', () => {
            if (httpResponse.statusCode === 200) resolve()
            else reject(new Error(`Got ${httpResponse.statusCode} from MatterMost! Body:\n${body}`))
        })
    })

    request.write(requestBody)
    request.end()
})


const executeBot = () => {
    makePostsDirectory
        .then(() => Facecuck.api('oauth/access_token', Object.assign({}, {grant_type: 'client_credentials'}, credentials)))
        .then(response => Facecuck.api(`${INFOPIGULA_PAGE_ID}/posts?limit=5`, response)) // we get 5 latest posts only!
        .then(fbResponse => {
            const posts = fbResponse.data
            const saveAllPostsPromises = posts.map(postObj =>
                checkIfPostExists(postObj.id)
                    .then(exists => {
                        if (exists) {
                            return Promise.resolve(undefined) // already published, mark as missing
                        } else {
                            return savePost(postObj.id, postObj.message)
                                .then(() => Promise.resolve(postObj)) // to publish, return post object
                        }
                    })
            )

            return Promise.all(saveAllPostsPromises)
        })
        .then(results => {
            const newPosts = results.filter(result => result !== undefined) // drop posts already published to #InfoPiguła

            // sort posts from oldest to newest
            newPosts.sort((a, b) => new Date(a.created_time).getTime() - new Date(b.created_time).getTime())

            const concat = (x, y) => x.concat(y) // reducer function for two arrays, flattener

            // split full text messages to arrays of arrays of messages smaller than acceptable MatterMost message limit
            // then map them to functions returning promises of HTTP requests to MatterMost API hook (deferred promises)
            // then flatten arrays of arrays of functions returning promises to an array of functions returning promises
            const matterMostSendPromises =
                newPosts.map(postObj => ({
                    date: postObj.created_time,
                    id: postObj.id,
                    messages: splitTextToMattermostLimitedMessages(postObj.message)
                })).map(obj =>
                    obj.messages.map(message => () => sendMessageToMattermost(message).then(() => ({id: obj.id, date: obj.date})))
                ).reduce(concat, [])

            return Promise.sequence(matterMostSendPromises) // resolve promises sequentially to guarantee posting order
        })
        .then(postsSentToMattermost => {
            postsSentToMattermost.forEach(postObj => {
                console.log(`Published post with id ${postObj.id} (posted to FB at: ${postObj.date}) to Mattermost successfully.`)
            })
        })
        .catch(console.error)
}

// run bot daily
setInterval(executeBot, 1000 * 60 * 60 * 24)

// first run
executeBot()